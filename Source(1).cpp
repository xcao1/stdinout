#include<cstdio>
#include<cctype>
#include<cmath>
#include<vector>
using namespace std;
enum READ_STATE{
	READING,
	WHITESPACE,
	OTHER
};
int main(){
	//FILE *doc = fopen("numbers.txt","r");
	vector<double> my_numbers;
	double current_number = 0;
	READ_STATE my_read_state = READ_STATE::OTHER;
	bool negtive = false;
	bool decimal = false;
	int precise = 0;
	int num = 0;
	while(num != EOF){
		num = fgetc(stdin);
		if(isdigit(num)){// number
			int temp = num - '0';
			if(my_read_state != READ_STATE::READING){
				current_number = temp;
			}else{
				if(!decimal){
					current_number = current_number*10 + temp;
				}else{
					precise++;
					current_number = current_number + temp*pow(10,-1*precise);
				}
			}
			my_read_state = READ_STATE::READING;
		}else{// not number
			if(num == '-')
				negtive = true;
			else if(num == '.')
				decimal = true;
			else{
				if(my_read_state == READ_STATE::READING){
					if(negtive)
						current_number *= -1;
					my_numbers.push_back(current_number);
				}
				negtive = false;
				decimal = false;
				precise = 0;
				current_number = 0;
				if(num == ' ' || num == '\n'){// white space
					my_read_state = READ_STATE::WHITESPACE;
				}else{// other character
					my_read_state = READ_STATE::OTHER;
				}
			}
		}
	}
	//fclose(doc);
	vector<double>::iterator iter = my_numbers.begin();
	while(iter != my_numbers.end()){
		printf("%f\n",*iter);
		iter++;
	}

	printf("now, sorting the number....");
	int size = my_numbers.size();
	printf("%d\n",size);
	for(int i = 0; i < size - 1; i++){
		for(int j = 0; j < size - (i+1); j++){
			if(my_numbers[j]>my_numbers[j+1]){
				double temp = my_numbers[j];
				my_numbers[j] = my_numbers[j+1];
				my_numbers[j+1] = temp;
			}
		}
	}
	//doc = fopen("result.txt","w");
	iter = my_numbers.begin();
	while(iter != my_numbers.end()){
		//printf("%f\n",*iter);
		fprintf(stdout,"%f ",*iter);
		iter++;
	}
	//fclose(doc);


	system("pause");
	return 0;
}